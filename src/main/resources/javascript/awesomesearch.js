AJS.toInit(function ($) {
    var contextPath = document.getElementById('confluence-context-path').content;
    
    $("a.favourite-off,a.favourite-on").each(function() {
        var $this = $(this);
        $this.click(function() {
            var page = location.href;
            $.get($this.attr("href"), function() {
                location.href = page;
                location.reload();
            });
            return false;
        })
    })
    
    $(".search-result-title").each(function() {
        var $this = $(this);
        $this.click(function() {
            var handle = AJS.$(".search-result-entry-content-id", $this.parent()).attr("value");
            var queryString = AJS.$("#query-string").attr("value");
            var url = contextPath + "/awesomesearch/learn.action?handle=" + escape(handle) + "&queryString=" + escape(queryString);
            AJS.$.get(url);
            return true;
        })
    })
})