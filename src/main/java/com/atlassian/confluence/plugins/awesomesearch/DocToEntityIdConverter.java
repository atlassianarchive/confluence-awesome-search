package com.atlassian.confluence.plugins.awesomesearch;

import bucket.core.persistence.hibernate.HibernateHandle;

import com.atlassian.bonnie.search.BaseDocumentBuilder.FieldName;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.FieldCache;

import java.io.IOException;
import java.text.ParseException;

public class DocToEntityIdConverter
{
    public Long getEntityIdFromDoc(IndexReader reader, int doc) throws IOException
    {
        Long resultEntityId;
        String handle = FieldCache.DEFAULT.getStrings(reader, FieldName.HANDLE)[doc];
        try
        {
            HibernateHandle hibernateHandle = new HibernateHandle(handle);
            resultEntityId = hibernateHandle.getId();
        }
        catch (ParseException e)
        {
            resultEntityId = null;
        }
        return resultEntityId;
    }
}
