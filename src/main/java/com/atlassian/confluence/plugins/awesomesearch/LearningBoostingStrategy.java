package com.atlassian.confluence.plugins.awesomesearch;

import com.atlassian.confluence.plugins.awesomesearch.action.AwesomeSearchSettingsManager;
import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.search.v2.lucene.boosting.BoostingStrategy;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.index.TermEnum;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LearningBoostingStrategy implements BoostingStrategy
{
    private final AwesomeSearchSettingsManager awesomeSearchSettingsManager;

    public LearningBoostingStrategy(AwesomeSearchSettingsManager awesomeSearchSettingsManager)
    {
        this.awesomeSearchSettingsManager = awesomeSearchSettingsManager;
    }
    
    public float boost(IndexReader reader, int doc, float score) throws IOException
    {
        return score;
    }

    public float boost(IndexReader reader, SearchQueryParameters searchQueryParameters, int doc, float score) throws IOException
    {
        final String[] terms = searchQueryParameters.getQuery().split(LearningExtractor.SEPARATOR);

        List<String> termCollection = new ArrayList<String>();
        for (String term : terms)
        {
            String termPrefix = term + ":";
            Term prefixTerm = new Term(LearningExtractor.QUERY_TERMS_FIELD_KEY, termPrefix);
            TermEnum termEnum = reader.terms(prefixTerm);
            if (termEnum.term() != null)
            {
                do
                {
                    String termText = termEnum.term().text();
                    if (termText.startsWith(termPrefix))
                        termCollection.add(termText);
                    else
                        break;
                } while (termEnum.next());
            }
        }

        for (String term : termCollection)
        {
            TermDocs termDocs = reader.termDocs(new Term(LearningExtractor.QUERY_TERMS_FIELD_KEY, term));
            try
            {
                if (termDocs.skipTo(doc) && termDocs.doc() == doc)
                {
                    int count = Integer.parseInt(term.split(":")[1], 10);
                    score *= Math.log(Math.E + (count * getScoreIncrementMultiplier()));
                }
            }
            finally
            {
                termDocs.close();
            }
        }
        
        return score;
    }

    public float boost(IndexReader indexReader, Map<String, Object> stringObjectMap, int doc, float score) throws IOException
    {
        return boost(indexReader, doc, score);
    }

    private float getScoreIncrementMultiplier()
    {
        return this.awesomeSearchSettingsManager.getLearningBoostPercentage();
    }
    
}