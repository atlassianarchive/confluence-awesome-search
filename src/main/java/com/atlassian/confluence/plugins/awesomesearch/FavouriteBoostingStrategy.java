package com.atlassian.confluence.plugins.awesomesearch;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.search.lucene.LabelExtractor;
import com.atlassian.confluence.search.v2.lucene.boosting.BoostingStrategy;
import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.plugins.awesomesearch.action.AwesomeSearchSettingsManager;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import java.io.IOException;
import java.util.Map;

public class FavouriteBoostingStrategy implements BoostingStrategy
{
    private final AwesomeSearchSettingsManager awesomeSearchSettingsManager;

    public FavouriteBoostingStrategy(AwesomeSearchSettingsManager awesomeSearchSettingsManager)
    {
        this.awesomeSearchSettingsManager = awesomeSearchSettingsManager;
    }

    public float boost(IndexReader reader,  int doc, float score) throws IOException
    {
        String currentUser = AuthenticatedUserThreadLocal.getUsername();
        if (currentUser != null)
        {
            Label favouriteLabel = new Label(LabelManager.FAVOURITE_LABEL, Namespace.PERSONAL, currentUser);
            Term favouriteTerm = new Term(LabelExtractor.LABEL_FIELD, favouriteLabel.toStringWithOwnerPrefix());
            TermDocs termDocs = reader.termDocs(favouriteTerm);
            try
            {
                if (termDocs.skipTo(doc) && termDocs.doc() == doc)
                {
                    final float boostFactor = (1 + awesomeSearchSettingsManager.getFavouriteBoostPercentage() * 0.01f);
                    score = score * boostFactor;
                }
            }
            finally
            {
                termDocs.close();
            }
        }
        return score;
    }

    public float boost(IndexReader reader, SearchQueryParameters searchQueryParameters, int doc, float score) throws IOException
    {
        return boost(reader, doc, score);
    }

    public float boost(IndexReader indexReader, Map<String, Object> stringObjectMap, int doc, float score) throws IOException
    {
        return boost(indexReader, doc, score);
    }

}
