package com.atlassian.confluence.plugins.awesomesearch.action;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import org.apache.log4j.Logger;

public class AwesomeSearchSettingsManager
{
    private static final Logger log = Logger.getLogger(AwesomeSearchSettingsManager.class);
    
    private static final int DEFAULT_LEARNING_PERCENTAGE = 25;
    private static final int DEFAULT_FAVOURITE_PERCENTAGE = 40;
    private static final int DEFAULT_HISTORY_PERCENTAGE = 30;
    private static final int DEFAULT_CONTRIBUTION_PERCENTAGE = 20;
    
    private static final String SETTINGS_PREFIX = "com.atlassian.confluence.plugins.awesomesearch.";
    private static final String LEARNING_PERCENTAGE_KEY = SETTINGS_PREFIX + "learningBoostingPercentage";
    private static final String FAVOURITE_PERCENTAGE_KEY = SETTINGS_PREFIX + "favouriteBoostingPercentage";
    private static final String HISTORY_PERCENTAGE_KEY = SETTINGS_PREFIX + "historyBoostingPercentage";
    private static final String CONTRIBUTION_PERCENTAGE_KEY = SETTINGS_PREFIX + "contributionBoostingPercentage";
    private PluginSettings pluginSettings;

    public AwesomeSearchSettingsManager(PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }
    
    public Integer getFavouriteBoostPercentage()
    {
        Integer result = getSetting(FAVOURITE_PERCENTAGE_KEY);
        return result != null ? result : defaultFavouritePercentage();
    }

    public Integer getLearningBoostPercentage()
    {
        Integer result = getSetting(LEARNING_PERCENTAGE_KEY);
        return result != null ? result : defaultLearningPercentage();
    }
    
    public Integer getHistoryBoostPercentage()
    {
        Integer result = getSetting(HISTORY_PERCENTAGE_KEY);
        return result != null ? result : defaultHistoryPercentage();
    }
    
    public Integer getContributionBoostPercentage()
    {
        Integer result = getSetting(CONTRIBUTION_PERCENTAGE_KEY);
        return result != null ? result : defaultContributionPercentage();
    }
    
    private int defaultLearningPercentage()
    {
        setLearningBoostPercentage(DEFAULT_LEARNING_PERCENTAGE);
        return DEFAULT_LEARNING_PERCENTAGE;
    }
    
    private int defaultFavouritePercentage()
    {
        setFavouriteBoostPercentage(DEFAULT_FAVOURITE_PERCENTAGE);
        return DEFAULT_FAVOURITE_PERCENTAGE;
    }
    
    private int defaultHistoryPercentage()
    {
        setHistoryBoostPercentage(DEFAULT_HISTORY_PERCENTAGE);
        return DEFAULT_HISTORY_PERCENTAGE;
    }
    
    private int defaultContributionPercentage()
    {
        setContributionBoostPercentage(DEFAULT_CONTRIBUTION_PERCENTAGE);
        return DEFAULT_CONTRIBUTION_PERCENTAGE;
    }

    public void setLearningBoostPercentage(int percentage)
    {
        store(LEARNING_PERCENTAGE_KEY, percentage);
    }

    public void setFavouriteBoostPercentage(int percentage)
    {
        store(FAVOURITE_PERCENTAGE_KEY, percentage);
    }

    public void setHistoryBoostPercentage(Integer percentage)
    {
        store(HISTORY_PERCENTAGE_KEY, percentage);
    }
    
    public void setContributionBoostPercentage(Integer percentage)
    {
        store(CONTRIBUTION_PERCENTAGE_KEY, percentage);
    }
    
    private Integer getSetting(String key)
    {
        Object object = pluginSettings.get(key);
        if(object != null && object instanceof String)
        {
            try
            {
                return Integer.parseInt((String) object, 10);
            }
            catch(NumberFormatException e)
            {
                log.warn(e);
            }
        }
        return null;
    }

    private void store(String key, int value)
    {
        pluginSettings.put(key, String.valueOf(value));
    }
    
}
