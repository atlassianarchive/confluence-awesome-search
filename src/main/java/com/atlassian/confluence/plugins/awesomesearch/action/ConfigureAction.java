package com.atlassian.confluence.plugins.awesomesearch.action;

import com.atlassian.confluence.core.ConfluenceActionSupport;

public class ConfigureAction extends ConfluenceActionSupport
{
    private AwesomeSearchSettingsManager awesomeSearchSettingsManager;
    private Integer learningBoostPercentage;
    private Integer favouriteBoostPercentage;
    private Integer historyBoostPercentage;
    private Integer contributionBoostPercentage;

    @Override
    public String execute()
    {
        if (learningBoostPercentage == null)
        {
            return INPUT;
        }
        
        awesomeSearchSettingsManager.setLearningBoostPercentage(learningBoostPercentage);
        awesomeSearchSettingsManager.setFavouriteBoostPercentage(favouriteBoostPercentage);
        awesomeSearchSettingsManager.setHistoryBoostPercentage(historyBoostPercentage);
        awesomeSearchSettingsManager.setContributionBoostPercentage(contributionBoostPercentage);
        
        return SUCCESS;
    }
    
    public void setAwesomeSearchSettingsManager(AwesomeSearchSettingsManager awesomeSearchSettingsManager)
    {
        this.awesomeSearchSettingsManager = awesomeSearchSettingsManager;
    }
    
    public int getLearningBoost()
    {
        return awesomeSearchSettingsManager.getLearningBoostPercentage();
    }
    
    public void setLearningBoost(int percentage)
    {
        this.learningBoostPercentage = percentage;
    }

    public int getFavouriteBoost()
    {
        return awesomeSearchSettingsManager.getFavouriteBoostPercentage();
    }
    
    public void setFavouriteBoost(int percentage)
    {
        this.favouriteBoostPercentage = percentage;
    }
    
    public void setHistoryBoost(int percentage)
    {
        this.historyBoostPercentage = percentage;
    }
    
    public int getHistoryBoost()
    {
        return awesomeSearchSettingsManager.getHistoryBoostPercentage();
    }
    
    public void setContributionBoost(int percentage)
    {
        this.contributionBoostPercentage = percentage;
    }
    
    public int getContributionBoost()
    {
        return awesomeSearchSettingsManager.getContributionBoostPercentage();
    }
}
