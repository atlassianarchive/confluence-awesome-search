package com.atlassian.confluence.plugins.awesomesearch;

import com.atlassian.bonnie.search.Extractor;
import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class LearningExtractor implements Extractor
{
    private final ContentPropertyManager contentPropertyManager;
    public static final String QUERY_TERMS_FIELD_KEY = "queryTerms";
    public static final String CONTENT_PROPERTY_KEY = LearningExtractor.class.getCanonicalName() + ":" + QUERY_TERMS_FIELD_KEY;
    public static final String SEPARATOR = " ";

    public LearningExtractor(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    public void addFields(Document document, StringBuffer not_used, Searchable searchable)
    {
        if (!(searchable instanceof ContentEntityObject))
        {
            return;
        }

        ContentEntityObject content = (ContentEntityObject) searchable;
        if (StringUtils.isBlank(content.getTitle()))
        {
            return;
        }
        
        String queryTermsBulk = contentPropertyManager.getTextProperty(content, CONTENT_PROPERTY_KEY);
        
        if(!StringUtils.isBlank(queryTermsBulk))
        {
            String[] terms = queryTermsBulk.split(SEPARATOR);
            
            Set<Entry<String, Integer>> termEntries = collectTerms(terms);
            for (Entry<String, Integer> termEntry : termEntries)
            {
                final Field field = new Field(QUERY_TERMS_FIELD_KEY, termEntry.getKey() + ":" + termEntry.getValue(), Field.Store.NO, Field.Index.UN_TOKENIZED);
                document.add(field);
            }
        }
       
    }

    private static Set<Entry<String, Integer>> collectTerms(String[] terms)
    {
        final Map<String, Integer> map = new HashMap<String, Integer>();
        for (String term : terms)
        {
            Integer i = map.get(term);
            if(i == null)
            {
                map.put(term, 1);
                continue;
            }
            else map.put(term, 1+i);
        }
        return map.entrySet();
    }
    
}
