package com.atlassian.confluence.plugins.awesomesearch.action;

import bucket.core.persistence.hibernate.HibernateHandle;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.persistence.AnyTypeDao;

import org.apache.log4j.Logger;

import java.text.ParseException;

public class AwesomeSearchAction extends ConfluenceActionSupport implements Beanable
{
    private static final Logger log = Logger.getLogger(AwesomeSearchAction.class);
    
    private final AnyTypeDao anyTypeDao;
    private final QueryHistoryBuilder queryHistoryBuilder;
    
    private String queryString;
    private String handle;

    public AwesomeSearchAction(AnyTypeDao anyTypeDao, QueryHistoryBuilder queryHistoryBuilder)
    {
        this.anyTypeDao = anyTypeDao;
        this.queryHistoryBuilder = queryHistoryBuilder;
    }

    @Override
    public String execute()
    {
        try
        {
            final HibernateHandle handle = new HibernateHandle(this.handle);
            final Object o = anyTypeDao.findByHandle(handle);
            if(!(o instanceof ContentEntityObject))
            {
                return doLog("Object was not a content entity object. handle: " + handle);
            }
            ContentEntityObject ceo = (ContentEntityObject) o;
            queryHistoryBuilder.add(ceo, queryString);
        }
        catch(ParseException e)
        {
            return doLog("Could not parse handle: " + handle);
        }
        return SUCCESS;
    }

    private String doLog(String msg)
    {
        log.info(msg);
        return SUCCESS;
    }
    
    public void setHandle(String handle)
    {
        this.handle = handle;
    }
    
    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    public Object getBean()
    {
        return null;
    }
}
