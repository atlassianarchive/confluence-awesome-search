package com.atlassian.confluence.plugins.awesomesearch;

import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.search.v2.lucene.boosting.BoostingStrategy;
import com.atlassian.confluence.plugins.awesomesearch.action.AwesomeSearchSettingsManager;

import org.apache.lucene.index.IndexReader;

import java.io.IOException;
import java.util.Map;

public class UserHistoryBoostingStrategy implements BoostingStrategy
{
    private DocToEntityIdConverter docToEntityIdConverter;
    private UserHistoryProvider userHistoryProvider;
    private final AwesomeSearchSettingsManager awesomeSearchSettingsManager;
    
    public UserHistoryBoostingStrategy(DocToEntityIdConverter docToEntityIdConverter,
                                       UserHistoryProvider userHistoryProvider, AwesomeSearchSettingsManager awesomeSearchSettingsManager)
    {
        this.docToEntityIdConverter = docToEntityIdConverter;
        this.userHistoryProvider = userHistoryProvider;
        this.awesomeSearchSettingsManager = awesomeSearchSettingsManager;
    }

    public float boost(IndexReader reader, int doc, float score) throws IOException
    {
        Long resultEntityId = docToEntityIdConverter.getEntityIdFromDoc(reader, doc);

        if (resultEntityId == null)
            return score;
        
        for (Long entityId : userHistoryProvider.getUserHistory())
        {
            if (resultEntityId.longValue() == entityId)
            {
                final float boostFactor = (1 + awesomeSearchSettingsManager.getHistoryBoostPercentage() * 0.01f);
                score = score * boostFactor;
                break;
            }
        }
        return score;
    }

    public float boost(IndexReader reader, SearchQueryParameters searchQueryParameters, int doc, float score) throws IOException
    {
        return boost(reader, doc, score);
    }

    public float boost(IndexReader indexReader, Map<String, Object> stringObjectMap, int doc, float score) throws IOException
    {
        return boost(indexReader, doc, score);
    }

}
