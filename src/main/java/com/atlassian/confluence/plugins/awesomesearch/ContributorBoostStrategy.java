package com.atlassian.confluence.plugins.awesomesearch;

import com.atlassian.confluence.search.lucene.extractor.ContentEntityMetadataExtractor;
import com.atlassian.confluence.search.v2.lucene.boosting.BoostingStrategy;
import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.plugins.awesomesearch.action.AwesomeSearchSettingsManager;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;

import java.io.IOException;
import java.util.Map;

public class ContributorBoostStrategy implements BoostingStrategy
{
    private final AwesomeSearchSettingsManager awesomeSearchSettingsManager;

    public ContributorBoostStrategy(AwesomeSearchSettingsManager awesomeSearchSettingsManager)
    {
        this.awesomeSearchSettingsManager = awesomeSearchSettingsManager;
    }

    /**
     * Apply a relevant boost to the specified document with the specified score. Returning a score
     * of 0 will remove the document from the results.
     *
     * @param reader a reader instance associated with the current scoring process
     * @param doc    the doc id
     * @param score  the original score for the document specified by doc   @return the boosted score or 0 to remove the document from the results
     */
    public float boost(IndexReader reader, int doc, float score) throws IOException
    {
        String currentUser = AuthenticatedUserThreadLocal.getUsername();
        if(currentUser != null)
        {
            TermDocs termDocs = reader.termDocs(new Term(ContentEntityMetadataExtractor.LAST_MODIFIERS_FIELD, currentUser));
            try
            {
                if(termDocs.skipTo(doc) && termDocs.doc() == doc)
                {
                    final float boostFactor = (1 + awesomeSearchSettingsManager.getContributionBoostPercentage() * 0.01f);
                    score = score * boostFactor;
                }
            }
            finally
            {
                termDocs.close();
            }
        }
        return score;
    }

    public float boost(IndexReader indexReader, Map<String, Object> stringObjectMap, int doc, float score) throws IOException
    {
        return boost(indexReader, doc, score);
    }

    public float boost(IndexReader reader, SearchQueryParameters searchQueryParameters, int doc, float score) throws IOException
    {
        return boost(reader, doc, score);
    }

}
