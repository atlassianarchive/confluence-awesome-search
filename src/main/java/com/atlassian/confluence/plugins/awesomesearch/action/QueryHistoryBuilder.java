package com.atlassian.confluence.plugins.awesomesearch.action;

import static com.atlassian.confluence.plugins.awesomesearch.LearningExtractor.CONTENT_PROPERTY_KEY;
import static com.atlassian.confluence.plugins.awesomesearch.LearningExtractor.SEPARATOR;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.search.ConfluenceIndexer;

public class QueryHistoryBuilder
{
    private final ContentPropertyManager contentPropertyManager;
    private final ConfluenceIndexer confluenceIndexer;
    private final int maxhistorySize;

    public QueryHistoryBuilder(ContentPropertyManager contentPropertyManager, ConfluenceIndexer confluenceIndexer,
        int maxhistorySize)
    {
        this.contentPropertyManager = contentPropertyManager;
        this.confluenceIndexer = confluenceIndexer;
        this.maxhistorySize = maxhistorySize;
    }

    public void add(ContentEntityObject ceo, String queryTerms)
    {
        if (queryTerms.length() > maxhistorySize)
            return;
        
        String property = contentPropertyManager.getTextProperty(ceo, CONTENT_PROPERTY_KEY);
        StringBuilder sb = new StringBuilder(property == null ? "" : property);
        if (sb.length() > 0)
        {
            sb.append(SEPARATOR);
        }
        sb.append(queryTerms);
        if (sb.length() > maxhistorySize)
        {
            sb.delete(0, sb.indexOf(SEPARATOR, sb.length() - maxhistorySize) + 1);
        }
        contentPropertyManager.setTextProperty(ceo, CONTENT_PROPERTY_KEY, sb.toString());
        confluenceIndexer.reIndex(ceo);
    }
    
}
