package com.atlassian.confluence.plugins.awesomesearch;

import com.atlassian.confluence.user.SessionKeys;
import com.atlassian.confluence.user.history.UserHistory;
import com.opensymphony.xwork.ActionContext;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class UserHistoryProvider
{
    public List<Long> getUserHistory()
    {
        UserHistory history = getUserHistoryFromSession();
        if (history == null)
            return Collections.<Long>emptyList();

        return getContentIds(history);
    }

    private UserHistory getUserHistoryFromSession()
    {
        ActionContext context = ActionContext.getContext();
        if (context == null)
            return null;
        
        Map<?,?> session = context.getSession();
        if (session == null)
            return null;
        
        return (UserHistory) session.get(SessionKeys.USER_ISSUE_HISTORY);
    }

    @SuppressWarnings("unchecked")
    private List<Long> getContentIds(UserHistory history)
    {
        return history.getContent();
    }
}
