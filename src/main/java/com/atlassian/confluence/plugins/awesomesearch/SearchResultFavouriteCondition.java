package com.atlassian.confluence.plugins.awesomesearch;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.conditions.BaseConfluenceCondition;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.user.User;

public class SearchResultFavouriteCondition extends BaseConfluenceCondition
{
    @Override
    protected boolean shouldDisplay(WebInterfaceContext context)
    {
        SearchResult searchResult = (SearchResult) context.getParameter("searchResult");
        User user = context.getUser();
        if (searchResult != null && user != null)
        {
            Label label = new Label(LabelManager.FAVOURITE_LABEL, Namespace.PERSONAL, user.getName());
            return searchResult.getPersonalLabels().contains(label.toStringWithOwnerPrefix());
        }
        return false;
    }
}
