package com.atlassian.confluence.plugins.awesomesearch;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

public class TestLearningExtractor extends TestCase
{
    @Mock private ContentPropertyManager contentPropertyManager;
    @Mock private ContentEntityObject searchableCEO;
    @Mock private Searchable searchable;
    
    private Document document;
    private LearningExtractor extractor;

    @Override
    public void setUp()
    {
        initMocks(this);
        document = new Document();
        extractor = new LearningExtractor(contentPropertyManager);
    }
    
    public void testAddFieldsNoCEO()
    {
        extractor.addFields(document, null, searchable);
        
        assertEquals(0, document.getFields().size());
    }
    
    public void testAddFieldsNoPage()
    {
        extractor.addFields(document, null, searchableCEO);
        
        verify(contentPropertyManager, never()).getTextProperty(searchableCEO, LearningExtractor.CONTENT_PROPERTY_KEY);
        assertEquals(0, document.getFields().size());
    }
    
    public void testAddFields()
    {
        when(searchableCEO.getTitle()).thenReturn("non empty string");
        when(contentPropertyManager.getTextProperty(searchableCEO, LearningExtractor.CONTENT_PROPERTY_KEY)).thenReturn("foo bar foo");
        
        extractor.addFields(document, null, searchableCEO);
        
        assertValuesInDocument(document, "foo:2", "bar:1");
    }
    
    private void assertValuesInDocument(Document document, String... terms)
    {
        Field[] fields = document.getFields(LearningExtractor.QUERY_TERMS_FIELD_KEY);

        assertEquals(fields.length, terms.length);
        
        Set<String> actualTerms = new HashSet<String>();
        for (Field field : fields)
        {
            actualTerms.add(field.stringValue());
        }
        
        assertEquals(new HashSet<String>(Arrays.asList(terms)), actualTerms);
    }    
}
