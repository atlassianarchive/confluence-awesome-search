package com.atlassian.confluence.plugins.awesomesearch;

import junit.framework.TestCase;

import java.io.IOException;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.plugins.awesomesearch.action.AwesomeSearchSettingsManager;

public class TestLearningBoostingStrategy extends TestCase
{
    private LearningBoostingStrategy strategy;
    private Directory directory;
    
    @Mock SearchQueryParameters searchQueryParameters;
    @Mock AwesomeSearchSettingsManager awesomeSearchSettingsManager;
    
    public void setUp()
    {
        initMocks(this);
        
        when(awesomeSearchSettingsManager.getLearningBoostPercentage()).thenReturn(25);
        
        strategy = new LearningBoostingStrategy(awesomeSearchSettingsManager);
        directory = new RAMDirectory();
    }
    
    public void testScoringWithoutQueryParameters() throws IOException
    {
        float expected = 1.2f;
        float score = strategy.boost(null, 0, expected);
        assertEquals("Score should not be affected.", score, expected);
    }
    
    public void testScoringWithQueryParameters() throws IOException
    {
        Document document = new Document();
        addField(document, "giant:2");
        addField(document, "head:3");

        addDocuments(document);
        
        IndexReader indexReader = getIndexReader();
        try
        {
            when(searchQueryParameters.getQuery()).thenReturn("elephant head dress");
    
            final float originalScore = 1.0f;
            float result = strategy.boost(indexReader, searchQueryParameters, 0, originalScore);
                    
            assertTrue("Result should have been increased.", result > originalScore);
        }
        finally
        {
            indexReader.close();
        }
    }

    public void testScoringWithQueryParametersAndNoFieldsInTheIndex() throws IOException
    {
        addDocuments(new Document());
        
        when(searchQueryParameters.getQuery()).thenReturn("elephant head dress");

        IndexReader indexReader = getIndexReader();
        try
        {
            final float originalScore = 1.0f;
            float result = strategy.boost(indexReader, searchQueryParameters, 0, originalScore);
            assertEquals("Result should have been increased.", 1.0f, result);
        }
        finally
        {
            indexReader.close();
        }
    }

    private void addDocuments(Document... documents) throws CorruptIndexException, LockObtainFailedException, IOException
    {
        IndexWriter indexWriter = new IndexWriter(directory, new StandardAnalyzer(), true);
        try
        {
            for (Document document : documents)
            {
                indexWriter.addDocument(document);
            }
        }
        finally
        {
            indexWriter.close();
        }
    }

    private void addField(Document document, String value)
    {
        document.add(new Field(LearningExtractor.QUERY_TERMS_FIELD_KEY, value, Field.Store.YES, Field.Index.UN_TOKENIZED));
    }

    private IndexReader getIndexReader() throws CorruptIndexException, IOException
    {
        return IndexReader.open(directory);
    }
    
}
