package com.atlassian.confluence.plugins.awesomesearch.action;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.plugins.awesomesearch.LearningExtractor;
import com.atlassian.confluence.search.ConfluenceIndexer;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import junit.framework.TestCase;

public class TestQueryHistoryBuilder extends TestCase
{
    @Mock private ContentPropertyManager contentPropertyManager;
    @Mock private ConfluenceIndexer confluenceIndexer;
    @Mock private ContentEntityObject contentEntityObject;
    
    private QueryHistoryBuilder queryHistoryBuilder;

    @Override
    protected void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);

        queryHistoryBuilder = new QueryHistoryBuilder(contentPropertyManager, confluenceIndexer, 10);
    }
    
    public void testAdd() throws Exception
    {
        queryHistoryBuilder.add(contentEntityObject, "foo");
        
        verify(contentPropertyManager).setTextProperty(contentEntityObject, LearningExtractor.CONTENT_PROPERTY_KEY, "foo");
        verify(confluenceIndexer).reIndex(contentEntityObject);
    }
    
    public void testAddSecondTerm() throws Exception
    {
        when(contentPropertyManager.getTextProperty(contentEntityObject, LearningExtractor.CONTENT_PROPERTY_KEY)).thenReturn("foo");
        
        queryHistoryBuilder.add(contentEntityObject, "bar");
        
        verify(contentPropertyManager).setTextProperty(contentEntityObject, LearningExtractor.CONTENT_PROPERTY_KEY, "foo bar");
        verify(confluenceIndexer).reIndex(contentEntityObject);
    }

    public void testAddMoreThanMax() throws Exception
    {
        when(contentPropertyManager.getTextProperty(contentEntityObject, LearningExtractor.CONTENT_PROPERTY_KEY)).thenReturn("foo bar");
        
        queryHistoryBuilder.add(contentEntityObject, "foo");
        
        verify(contentPropertyManager).setTextProperty(contentEntityObject, LearningExtractor.CONTENT_PROPERTY_KEY, "bar foo");
        verify(confluenceIndexer).reIndex(contentEntityObject);
    }

    public void testAddQuerTermLongerThanMax() throws Exception
    {
        queryHistoryBuilder.add(contentEntityObject, "foofoofoofoo");
        
        verify(contentPropertyManager, never()).setTextProperty(same(contentEntityObject), eq(LearningExtractor.CONTENT_PROPERTY_KEY), anyString());
        verify(confluenceIndexer, never()).reIndex(contentEntityObject);
    }

}
