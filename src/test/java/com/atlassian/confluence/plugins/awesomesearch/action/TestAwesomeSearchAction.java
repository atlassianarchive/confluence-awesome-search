package com.atlassian.confluence.plugins.awesomesearch.action;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import bucket.core.persistence.hibernate.HibernateHandle;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.persistence.AnyTypeDao;

import org.mockito.Mock;

import junit.framework.TestCase;

public class TestAwesomeSearchAction extends TestCase
{
    @Mock private AnyTypeDao anyTypeDao;
    @Mock private ContentEntityObject contentEntityObject;
    @Mock private QueryHistoryBuilder queryHistoryBuilder;

    private AwesomeSearchAction awesomeSearchAction;
    private HibernateHandle hibernateHandle;

    public void setUp() throws Exception
    {
        super.setUp();
        initMocks(this);
        final String handleString = "dummy handle-1";
        hibernateHandle = new HibernateHandle(handleString);
        when(anyTypeDao.findByHandle(hibernateHandle)).thenReturn(contentEntityObject);
        awesomeSearchAction = new AwesomeSearchAction(anyTypeDao, queryHistoryBuilder);
        awesomeSearchAction.setHandle(handleString);
    }

    public void testAddSearchTerms()
    {
        awesomeSearchAction.setQueryString("hello hippy");
        awesomeSearchAction.execute();
        verify(queryHistoryBuilder).add(contentEntityObject, "hello hippy");
    }
    
    public void testAddSearchTermsForNonCEO()
    {
        when(anyTypeDao.findByHandle(hibernateHandle)).thenReturn(new Object());
        awesomeSearchAction.setQueryString("");
        awesomeSearchAction.execute();
        verifyZeroInteractions(queryHistoryBuilder);
    }
    
}
