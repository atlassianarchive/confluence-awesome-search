package com.atlassian.confluence.plugins.awesomesearch;

import static org.mockito.Mockito.when;

import org.mockito.Mock;
import static org.mockito.MockitoAnnotations.*;

import java.util.Collections;

import junit.framework.TestCase;
import com.atlassian.confluence.plugins.awesomesearch.action.AwesomeSearchSettingsManager;

public class TestUserHistoryBoostingStrategy extends TestCase
{
    @Mock private UserHistoryProvider userHistoryProvider;
    @Mock private DocToEntityIdConverter docToEntityIdConverter;
    @Mock private AwesomeSearchSettingsManager awesomeSearchSettingsManager;
    
    private UserHistoryBoostingStrategy strategy;
    
    @Override
    protected void setUp() throws Exception
    {
        initMocks(this);
        
        when(awesomeSearchSettingsManager.getHistoryBoostPercentage()).thenReturn(30);
        strategy = new UserHistoryBoostingStrategy(docToEntityIdConverter, userHistoryProvider, awesomeSearchSettingsManager);
    }
    
    public void testBoostingIfDocumentIsInUsersHistory() throws Exception
    {
        when(docToEntityIdConverter.getEntityIdFromDoc(null, 0)).thenReturn(123l);
        when(userHistoryProvider.getUserHistory()).thenReturn(Collections.singletonList(123l));
        
        float result = strategy.boost(null, 0, 1.0f);
        
        assertEquals(1.3f, result);
    }

    public void testBoostingIfDocumentIsNotInUsersHistory() throws Exception
    {
        when(docToEntityIdConverter.getEntityIdFromDoc(null, 0)).thenReturn(123l);
        when(userHistoryProvider.getUserHistory()).thenReturn(Collections.<Long>emptyList());
        
        float result = strategy.boost(null, 0, 1.0f);
        
        assertEquals(1.0f, result);
    }
}
