package com.atlassian.confluence.plugins.awesomesearch;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations.Mock;
import static org.mockito.MockitoAnnotations.initMocks;

import com.atlassian.confluence.search.lucene.extractor.ContentEntityMetadataExtractor;
import com.atlassian.confluence.plugins.awesomesearch.action.AwesomeSearchSettingsManager;

import org.apache.lucene.index.Term;


public class TestContributorBoostStrategy extends AbstractBoostingStrategyTest
{
    @Mock private AwesomeSearchSettingsManager awesomeSearchSettingsManager;
    
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        when(awesomeSearchSettingsManager.getContributionBoostPercentage()).thenReturn(20);
    }
    
    public void testBoostIfCurrentUserIsNotContributor() throws Exception
    {
        when(reader.termDocs(new Term(ContentEntityMetadataExtractor.LAST_MODIFIERS_FIELD, USER_NAME))).thenReturn(termDocs);
        
        float result = strategy.boost(reader, 0, ORIGINAL_SCORE);
        
        assertEquals(ORIGINAL_SCORE, result);
        verify(termDocs).close();
    }

    public void testBoostIfCurrentUserIsContributor() throws Exception
    {
        when(reader.termDocs(new Term(ContentEntityMetadataExtractor.LAST_MODIFIERS_FIELD, USER_NAME))).thenReturn(termDocs);
        when(termDocs.skipTo(0)).thenReturn(true);
        when(termDocs.doc()).thenReturn(0);
        
        float result = strategy.boost(reader, 0, ORIGINAL_SCORE);
        
        assertEquals(ORIGINAL_SCORE * 1.2f, result);
        verify(termDocs).close();
    }

    @Override
    protected ContributorBoostStrategy createBoostingStrategy()
    {
        return new ContributorBoostStrategy(awesomeSearchSettingsManager);
    }
}
