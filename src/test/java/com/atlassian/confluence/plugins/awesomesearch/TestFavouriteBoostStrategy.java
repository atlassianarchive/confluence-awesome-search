package com.atlassian.confluence.plugins.awesomesearch;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations.Mock;
import static org.mockito.MockitoAnnotations.initMocks;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.search.lucene.LabelExtractor;
import com.atlassian.confluence.search.v2.lucene.boosting.BoostingStrategy;
import com.atlassian.confluence.plugins.awesomesearch.action.AwesomeSearchSettingsManager;

import org.apache.lucene.index.Term;

public class TestFavouriteBoostStrategy extends AbstractBoostingStrategyTest
{
    private Term favouriteTerm;
    @Mock private AwesomeSearchSettingsManager awesomeSearchSettingsManager;

    @Override
    protected void setUp() throws Exception
    {
        initMocks(this);
        super.setUp();
        
        Label favouriteLabel = new Label(LabelManager.FAVOURITE_LABEL, Namespace.PERSONAL, USER_NAME);
        favouriteTerm = new Term(LabelExtractor.LABEL_FIELD, favouriteLabel.toStringWithOwnerPrefix());
    }
    
    public void testBoostIfResultIsFavouriteOfCurrentUser() throws Exception
    {
        when(awesomeSearchSettingsManager.getFavouriteBoostPercentage()).thenReturn(40);
        when(reader.termDocs(favouriteTerm)).thenReturn(termDocs);
        when(termDocs.skipTo(0)).thenReturn(true);
        when(termDocs.doc()).thenReturn(0);
        
        float result = strategy.boost(reader, 0, ORIGINAL_SCORE);
        
        assertEquals(ORIGINAL_SCORE * 1.4f, result);
        verify(termDocs).close();
    }

    public void testBoostIfResultIsNotFavouriteOfCurrentUser() throws Exception
    {
        when(reader.termDocs(favouriteTerm)).thenReturn(termDocs);
        
        float result = strategy.boost(reader, 0, ORIGINAL_SCORE);
        
        assertEquals(ORIGINAL_SCORE, result);
        verify(termDocs).close();
    }

    @Override
    protected BoostingStrategy createBoostingStrategy()
    {
        return new FavouriteBoostingStrategy(awesomeSearchSettingsManager);
    }
}
