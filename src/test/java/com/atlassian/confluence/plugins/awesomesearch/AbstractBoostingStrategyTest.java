package com.atlassian.confluence.plugins.awesomesearch;

import static org.mockito.Mockito.when;

import com.atlassian.confluence.search.v2.lucene.boosting.BoostingStrategy;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.TermDocs;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import junit.framework.TestCase;

public abstract class AbstractBoostingStrategyTest extends TestCase
{
    protected static final float ORIGINAL_SCORE = 1.0f;
    protected static final String USER_NAME = "hannes";
    
    @Mock private User user;
    @Mock protected IndexReader reader;
    @Mock protected TermDocs termDocs;
    
    protected BoostingStrategy strategy;

    public AbstractBoostingStrategyTest()
    {
        super();
    }

    @Override
    protected void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
    
        strategy = createBoostingStrategy();
    
        when(user.getName()).thenReturn(USER_NAME);
        AuthenticatedUserThreadLocal.setUser(user);
    }

    @Override
    protected void tearDown() throws Exception
    {
        AuthenticatedUserThreadLocal.reset();
    }

    abstract protected BoostingStrategy createBoostingStrategy();

}